<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Task::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($tasks = Task::create([
            'name' => $request->name,
            'tags' => json_encode($request->tags),
        ]))
            return response($tasks, 200);
        else
            return response('Error creating task. Check information and try again.', 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        if($task->delete())
            return response('Task removed correctly', 200);
        else
            return response('Error removing task. Try again later.', 422);
    }
}
