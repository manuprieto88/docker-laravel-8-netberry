FROM rwslinkman/php8-fpm-nginx:1.0.1

ENV COMPOSER_VERSION 2.0.13

RUN apt-get update && apt-get install curl -y 

# Install NODEJS & NPM
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash

RUN apt-get update && apt install nodejs -y

RUN npm install -g npm

# Install Composer
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
    && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
    && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
    && php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION} \
    && rm -rf /tmp/composer-setup.php

EXPOSE 9080

WORKDIR /usr/share/nginx/html
