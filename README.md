# docker-php8-mysql

This a docker for Laravel
=============

Added as service
- PHP8
- Mysql
- nginx:latest image

Customization:
- Composer: 2.0.13

The environment is created using: 
- Dockerfile 
- docker-compose.yml
- .env

Building
=============
### Build using Docker-Compose ###

- Docker compose will use .env file and Dockerfile to create containers

- Change your environment variables in .env file

- docker-compose build

- docker-compose up

Building process
-----------------

You can follow building process reading Dockerfile

There are two config files that are going to be copied:

### default.conf ###

For NGINX server configuration. Copied to /etc/nginx/conf.d/default.conf
You can change the default folder used by nginx
After changes use service nginx restart

NGINX Resources:
https://www.linode.com/docs/guides/how-to-configure-nginx/

Connecting to services
----------------------
### Mysql Database ###
docker exec -ti dockerphpmysql_mysql_db_1 bash

### Webserver ###
docker exec -ti dockerdevenvironment_nginx_web_1 bash

Run Project
--------------
- Copy laravel/netberry/.env-dev to laravel/netberry/.env
- Enter Webserver service 
    - Execute composer install 
    - Execute npm install, and npm run development
    - Execute php artisan migrate

- Open web browser http://localhost:9080